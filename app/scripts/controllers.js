'use strict';
angular.module('PonderRate.controllers', [])

  .controller('AppCtrl', function($scope, $state, $ionicModal) {
  })

  .controller('PonderCamCtrl', function($scope, $state, $ionicModal, ponderRateService) {

    navigator.camera.getPicture(onSuccess, onFail, { quality: 50,
      destinationType: Camera.DestinationType.DATA_URL,
      saveToPhotoAlbum: true
    });

    function onSuccess(imageData) {
      console.log("imageData",imageData);
      var ponderRateProducts = {};
      ponderRateProducts.imageData = imageData;
      ponderRateProducts.src = "data:image/jpeg;base64," + imageData;

      ponderRateService.lastProduct = ponderRateProducts;

      $state.go('app.ponderTag');
    }

    function onFail(message) {
      //alert('Failed because: ' + message);
      $state.go('app.ponderRate');
    }

  })
  .controller('PonderStormCtrl', function($scope, $ionicModal, $timeout, ponderRateService) {
    console.log("imageDataObject",ponderRateService.ponderRateProducts);
    console.log("imageDataObject",ponderRateService.ponderRateProducts.src);
    $scope.ponderRateProducts = ponderRateService.ponderRateProducts;
    $scope.lastProduct = ponderRateService.lastProduct;

  })
  .controller('PonderTagCtrl', function($scope, $state, ponderRateService) {
    console.log("ponderRateService.lastProduct",ponderRateService.lastProduct);

    $scope.lastProduct = ponderRateService.lastProduct;

    $scope.confirmTag = function (){
      var disccount = Math.random() * 100;
      $scope.lastProduct.disccount = disccount.toFixed(0);
      ponderRateService.addProduct($scope.lastProduct);
      $state.go('app.ponderStorm');
    }

  })
  .controller('PonderRateCtrl', function($scope, $ionicModal, $timeout, ponderRateService) {
    $scope.ponderRateProducts = ponderRateService.ponderRateProducts;
    $scope.lastProduct = ponderRateService.lastProduct;

  })

  .controller('LoginCtrl', function($scope, $ionicModal, $timeout) {
  })
  .controller('PlaylistsCtrl', function($scope) {
  $scope.playlists = [
    { title: 'Reggae', id: 1 },
    { title: 'Chill', id: 2 },
    { title: 'Dubstep', id: 3 },
    { title: 'Indie', id: 4 },
    { title: 'Rap', id: 5 },
    { title: 'Cowbell', id: 6 }
  ];
  })

  .controller('PlaylistCtrl', function($scope, $stateParams) {
  })
  .service('ponderRateService', function() {
    this.ponderRateProducts = [];
    this.lastProduct = {};
    this.addProduct = function(product){
      this.ponderRateProducts.push(product);
    }
  });
